#ifndef _DIAGRAM_3_H_
#define _DIAGRAM_3_H_
#include <iostream>

namespace Prog3_3 {
    class Diagram {
    private:
        static const int QUOTA = 10;
        int SZ;
        int top;
        char *ar;
    public:
        Diagram() :SZ(QUOTA), top(0), ar(new char[QUOTA]){}
        Diagram(char el) :SZ(QUOTA), top(1), ar(new char[QUOTA]){ ar[0] = el; }
        Diagram(const Diagram&);
        Diagram(Diagram&&);
        ~Diagram(){ delete[] ar; }
        Diagram &operator =(const Diagram &);
        Diagram &operator =(Diagram &&);
        int push(char);
        int pop(char &);
        int fill(char);
        int clear();
        friend std::ostream & operator <<(std::ostream &, const Diagram &);
        Diagram & operator +=(const Diagram &);
        int copy(int);
        int leftShift(int);
        int rightShift(int);
        [[nodiscard]] int getSize() const{ return top; }
        int getMaxSize(){ return SZ; }
        static std::string toBinary(char);
        int randomPush(int);
    };
}

#endif
