#include "gtest/gtest.h"
#include "../src/diagram_1.h"
#include "../src/diagram_2.h"
#include "../src/diagram_3.h"

TEST(DiagramConstructor, DefaultConstructor)
{
    Prog3_1::Diagram diagram1;
    ASSERT_EQ(10, diagram1.getMaxSize());
    Prog3_2::Diagram diagram2;
    ASSERT_EQ(10, diagram2.getMaxSize());
    Prog3_3::Diagram diagram3;
    ASSERT_EQ(10, diagram3.getMaxSize());
}

TEST(DiagramMethods1, Parameters)
{
    Prog3_1::Diagram dia1;
    char el;
    ASSERT_EQ(10, dia1.fill('X'));
    ASSERT_ANY_THROW(dia1.fill('X')); // уже заполнено
    ASSERT_EQ(0, dia1.clear());
    ASSERT_ANY_THROW(dia1.push('q')); // неверный символ
    ASSERT_EQ(1, dia1.push('0'));
    ASSERT_EQ(0, dia1.pop(el));
    ASSERT_EQ(10, dia1.randomPush(10));
    ASSERT_ANY_THROW(dia1.push('1')); // уже заполнено
    ASSERT_EQ(0, dia1.clear());
    ASSERT_EQ(2, dia1.randomPush(2));
    ASSERT_EQ(4, dia1.copy(1));
    ASSERT_ANY_THROW(dia1.copy(2)); // 4 + (4*2) > 10, переполнение
    ASSERT_EQ(10, dia1.getSize());
    ASSERT_EQ(0, dia1.clear());
    ASSERT_EQ(7, dia1.randomPush(7));
    ASSERT_EQ(2, dia1.leftShift(5));
    ASSERT_EQ(8, dia1.rightShift(6));
    ASSERT_ANY_THROW(dia1.rightShift(3)); // 8 + 3 > 10, переполнение
    ASSERT_EQ(0, dia1.clear());
}

TEST(DiagramMethods2, Parameters)
{
    Prog3_2::Diagram dia2;
    char el;
    ASSERT_EQ(10, dia2.fill('X'));
    ASSERT_ANY_THROW(dia2.fill('X')); // уже заполнено
    ASSERT_EQ(0, dia2.clear());
    ASSERT_ANY_THROW(dia2.push('q')); // неверный символ
    ASSERT_EQ(1, dia2.push('0'));
    ASSERT_EQ(0, dia2.pop(el));
    ASSERT_EQ(10, dia2.randomPush(10));
    ASSERT_ANY_THROW(dia2.push('1')); // уже заполнено
    ASSERT_EQ(0, dia2.clear());
    ASSERT_EQ(2, dia2.randomPush(2));
    ASSERT_EQ(4, dia2.copy(1));
    ASSERT_ANY_THROW(dia2.copy(2)); // 4 + (4*2) > 10, переполнение
    ASSERT_EQ(10, dia2.getSize());
    ASSERT_EQ(0, dia2.clear());
    ASSERT_EQ(7, dia2.randomPush(7));
    ASSERT_EQ(2, dia2.leftShift(5));
    ASSERT_EQ(8, dia2.rightShift(6));
    ASSERT_ANY_THROW(dia2.rightShift(3)); // 8 + 3 > 10, переполнение
    ASSERT_EQ(0, dia2.clear());
}

TEST(DiagramMethods3, Parameters)
{
    Prog3_3::Diagram dia3;
    char el;
    ASSERT_EQ(10, dia3.fill('X'));
    ASSERT_EQ(20, dia3.fill('X')); // размер увеличен до 20
    ASSERT_EQ(20, dia3.getMaxSize());
    ASSERT_EQ(0, dia3.clear());
    ASSERT_ANY_THROW(dia3.push('q')); // неверный символ
    ASSERT_EQ(1, dia3.push('0'));
    ASSERT_EQ(0, dia3.pop(el));
    ASSERT_EQ(15, dia3.randomPush(15));
    ASSERT_EQ(25, dia3.randomPush(10)); // размер увеличен до 30
    ASSERT_EQ(30, dia3.getMaxSize());
    ASSERT_EQ(0, dia3.clear());
    ASSERT_EQ(4, dia3.randomPush(4));
    ASSERT_EQ(16, dia3.copy(3));
    ASSERT_EQ(32, dia3.copy(1)); // размер увеличен до 40
    ASSERT_EQ(40, dia3.getMaxSize());
    ASSERT_EQ(32, dia3.getSize());
    ASSERT_EQ(0, dia3.clear());
    ASSERT_EQ(25, dia3.randomPush(25));
    ASSERT_EQ(19, dia3.leftShift(6));
    ASSERT_EQ(39, dia3.rightShift(20));
    ASSERT_EQ(41, dia3.rightShift(2)); // размер увеличен до 50
    ASSERT_EQ(50, dia3.getMaxSize());
    ASSERT_EQ(0, dia3.clear());
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
