#ifndef _DIAGRAM_2_H_
#define _DIAGRAM_2_H_
#include <iostream>

namespace Prog3_2 {
    class Diagram {
    private:
        static const int SZ = 10;
        int top;
        char ar[SZ]{};
    public:
        Diagram() :top(0){}
        Diagram(char el) :top(1){ ar[0] = el; }
        int push(char);
        int pop(char &);
        int fill(char);
        int clear();
        friend std::ostream & operator <<(std::ostream &, const Diagram &);
        Diagram & operator +=(const Diagram &);
        int copy(int);
        int leftShift(int);
        int rightShift(int);
        [[nodiscard]] int getSize() const{ return top; }
        static int getMaxSize(){ return SZ; }
        static std::string toBinary(char);
        int randomPush(int);
    };
}

#endif
