#include "diagram_1.h"

using namespace Prog3_1;
int main()
{
    Diagram dia;
    char ch;
    std::cout << "Diagram max size = " << Diagram::getMaxSize() << std::endl;
    try {
        dia.fill('X');
    }
    catch (const std::exception &msg) {
        std::cout << msg.what() << std::endl;
    }
    std::cout << "In diagram:" << std::endl;
    dia.print(std::cout);
    std::cout << "Clearing the diagram dia" << std::endl;
    dia.clear();
    std::cout << "Diagram is empty" << std::endl << std::endl;
    std::cout << "Enter a string of ASCII characters" << std::endl;
    try {
        while (std::cin >> std::noskipws >> ch) {
            if (ch == '\n')
                break;
            else {
                std::string binary = Diagram::toBinary(ch);
                for (char const &c: binary)
                    dia.push(c);
            }
        }
    }
    catch (const std::exception &msg) {
        std::cout << msg.what() << std::endl;
    }
    dia.print(std::cout);
    std::cout << "Clearing the diagram dia" << std::endl;
    dia.clear();
    std::cout << "Diagram is empty" << std::endl << std::endl;
    std::cin.clear();
    std::cin.sync();
    std::cout << "Enter the line\n"
                 "Attention, only the characters '0', '1', 'X' will be read" << std::endl;
    try {
        while (std::cin >> std::noskipws >> ch) {
            if (ch == '\n')
                break;
            else if (ch == '0' or ch == '1' or ch == 'X')
                dia.push(ch);
            else
                continue;
        }
    }
    catch (const std::exception &msg) {
        std::cout << msg.what() << std::endl;
    }
    std::cout << "In diagram:" << std::endl;
    dia.print(std::cout);
    std::cout << "Clearing the diagram dia" << std::endl;
    dia.clear();
    std::cout << "Diagram is empty" << std::endl << std::endl;

    Diagram newdia;
    std::cout << "Push into dia, newdia diagrams:" << std::endl;
    dia.randomPush(4);
    newdia.randomPush(4);
    std::cout << "In dia diagram:" << std::endl;
    dia.print(std::cout);
    std::cout << "In newdia diagram:" << std::endl;
    newdia.print(std::cout);
    std::cout << "Adding newdia to dia" << std::endl;
    try {
        dia.combine(newdia);
    }
    catch (const std::exception &msg) {
        std::cout << msg.what() << std::endl;
    }
    std::cout << "In dia diagram:" << std::endl;
    dia.print(std::cout);
    std::cout << "In newdia diagram:" << std::endl;
    newdia.print(std::cout);
    std::cout << "Copying newdia n times" << std::endl;
    try {
        newdia.copy(3);
    }
    catch (const std::exception &msg) {
        std::cout << msg.what() << std::endl;
    }
    std::cout << "In newdia diagram:" << std::endl;
    newdia.print(std::cout);
    std::cout << "In dia diagram:" << std::endl;
    dia.print(std::cout);
    try {
        dia.rightShift(4);
    }
    catch (const std::exception &msg) {
        std::cout << msg.what() << std::endl;
    }
    std::cout << "In dia diagram:" << std::endl;
    dia.print(std::cout);
    try {
        dia.leftShift(5);
    }
    catch (const std::exception &msg) {
        std::cout << msg.what() << std::endl;
    }
    std::cout << "In dia diagram:" << std::endl;
    dia.print(std::cout);
    std::cout << "Clearing the diagram dia" << std::endl;
    dia.clear();
    std::cout << "Diagram is empty" << std::endl << std::endl;
    std::cout << "Clearing the diagram newdia" << std::endl;
    newdia.clear();
    std::cout << "Diagram is empty" << std::endl << std::endl;
    return 0;
}
