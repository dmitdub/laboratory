#include "diagram_1.h"
#include <bitset>

namespace Prog3_1 {
    int Diagram::pop(char &el) {
        int rc = top - 1;
        if (top > 0)
            el = ar[--top];
        return rc;
    }

    int Diagram::push(char el) {
        if (el != '0' and el != '1' and el != 'X')
            throw std::invalid_argument("Invalid signal state");
        if (top < SZ)
            ar[top++] = el;
        else
            throw std::overflow_error("Diagram overflow!");
        return top;
    }

    int Diagram::fill(char el) {
        if (el != '0' and el != '1' and el != 'X')
            throw std::invalid_argument("Invalid signal state");
        for (int i = 0; i < getMaxSize(); ++i)
            push(el);
        return top;
    }

    int Diagram::clear() {
        char el;
        while (pop(el) >= 0)
            ;
        return top;
    }

    std::ostream & Diagram::print(std::ostream &s) const {
        if (top == 0)
            s << "Diagram is empty";
        else
            for (int i = 0; i < top; ++i)
                s << ar[i];
        s << std::endl << std::endl;
        return s;
    }

    Diagram & Diagram::combine(const Diagram &dia) {
        Diagram tmp1(dia), tmp2;
        char el;
        while (tmp1.pop(el) >= 0)
            tmp2.push(el);
        while (tmp2.pop(el) >= 0)
            push(el);
        return *this;
    }

    int Diagram::copy(int n) {
        int size = top;
        for (int i = 0; i < n; ++i)
            for (int j = 0; j < size; ++j)
                push(ar[j]);
        return top;
    }

    int Diagram::leftShift(int n) {
        std::cout << "Left shift dia; steps: " << n << std::endl;
        Diagram tmp;
        char el;
        while (pop(el) >= 0)
            tmp.push(el);
        int i = 0;
        while (tmp.pop(el) >= 0) {
            if (i >= n)
                push(el);
            ++i;
        }
        return top;
    }

    int Diagram::rightShift(int n) {
        std::cout << "Right shift dia; steps: " << n << std::endl;
        Diagram tmp;
        char el;
        while (pop(el) >= 0)
            tmp.push(el);
        for (int i = 0; i < n; ++i)
            push('X');
        while (tmp.pop(el) >= 0)
            push(el);
        return top;
    }

    std::string Diagram::toBinary(char el) {
        std::string binary;
        binary = std::bitset<8>(el).to_string();
        return binary;
    }

    int Diagram::randomPush(int n) {
        int x;
        for (int i = 0; i < n; ++i) {
            x = rand() % 10; // needs to be improved
            if (x <= 3)
                push('0');
            else if (x <= 7)
                push('1');
            else
                push('X');
        }
        return top;
    }
}
