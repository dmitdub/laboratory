#ifndef _DIAGRAM_1_H_
#define _DIAGRAM_1_H_
#include <iostream>

namespace Prog3_1 {
    class Diagram {
    private:
        static const int SZ = 10;
        int top;
        char ar[SZ]{};
    public:
        Diagram() :top(0){}
        int push(char);
        int pop(char &);
        int fill(char);
        int clear();
        std::ostream & print(std::ostream &) const;
        Diagram & combine(const Diagram &);
        int copy(int);
        int leftShift(int);
        int rightShift(int);
        [[nodiscard]] int getSize() const{ return top; }
        static int getMaxSize(){ return SZ; }
        static std::string toBinary(char);
        int randomPush(int);
    };
}

#endif
