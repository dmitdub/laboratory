#ifndef _TABLE_H_
#define _TABLE_H_
#include <iostream>
#include <map>
#include <string>
#include "product.h"

namespace Prog4 {
    std::ostream & operator <<(std::ostream &, const std::pair<const int, Products::Product *> &);

    class Table {
    private:
        std::map<const int, Products::Product *> arr;
    public:
        Table(){}
        Table(const Table &);
        ~Table();
        Table& operator = (const Table &);
        friend class ConstTableIt;
        typedef class ConstTableIt Const_Iterator;
        Const_Iterator find(const int &)const;
        bool insert(const int &, const Products::Product *);
        bool remove(const int &);
        Const_Iterator begin() const;
        Const_Iterator end() const;
    };

    class ConstTableIt{
    private:
        std::map<const int, Products::Product *>::const_iterator cur;
    public:
        ConstTableIt(){}
        ConstTableIt(std::map<const int, Products::Product *>::iterator it) :cur(it){}
        ConstTableIt(std::map<const int, Products::Product *>::const_iterator it) :cur(it){}
        int operator !=(const ConstTableIt &) const;
        int operator ==(const ConstTableIt &) const;
        const std::pair<const int, Products::Product *> & operator *();
        const std::pair<const int, Products::Product *> * operator ->();
        ConstTableIt & operator ++();
        ConstTableIt operator ++(int);
    };
}

#endif