#include <string>
#include <iostream>
#include "product.h"

using namespace Products;

const char *Sh[] = { "1. Wholesale", "2. Retail", "0. Quit" },
        *Choice = "Make your choice",
        *Msg = "You are wrong; repeat please";
const int NumSh = sizeof(Sh) / sizeof(Sh[0]);
int Answer(const char *alt[], int n);
Product *getProduct();

int main() {
    Product *ptr = nullptr;
    while(ptr = getProduct()){
        std::cout << "You entered a ";
        std::cout << (*ptr) << std::endl;
        std::cout << "It's volume is equal to " << (ptr->volume()) << std::endl;
        delete ptr;
    }
    std::cout << "That's all. Buy!" << std::endl;
    return 0;
}

int Answer(const char *alt[], int n)
{
    int answer;
    const char *prompt = Choice;
    std::cout << "What do you want to do:" << std::endl;
    for(int i = 0; i < n; i++)
        std::cout << alt[i] << std::endl;
    do{
        std::cout << prompt << ": -> ";
        prompt = Msg;
        std::cin >> answer;
        if (!std::cin.good()){
            std::cin.clear();
            std::cin.ignore(80, '\n');
        }
    } while(answer < 0 || answer >= n);
    std::cin.ignore(80, '\n');
    return answer;
}

Product * getProduct() {
    Product *ptr = nullptr;
    int ans = Answer(Sh, NumSh);
    if (ans == 0)
        return ptr;
    switch (ans) {
        case 1:
            std::cout << "Enter values:\nstring(name) int(quantity) string(firm) string (country) int(price) int(wSize)\n--> ";
            ptr = new Wholesale;
            break;
        case 2:
            std::cout << "Enter values:\nstring(name) int(quantity) string(firm) string (country) int(price) int(markup)\n--> ";
            ptr = new Retail;
            break;
    }
    std::cin >> (*ptr);
    std::cin.ignore(80, '\n');
    return ptr;
}