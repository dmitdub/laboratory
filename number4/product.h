#ifndef _PRODUCT_H_
#define _PRODUCT_H_
#include <iostream>

namespace Products {
    class Product {
    protected:
        virtual std::ostream& show(std::ostream&)const = 0;
        virtual std::istream& get(std::istream&) = 0;
    public:
        virtual Product* clone()const = 0;
        friend std::ostream& operator <<(std::ostream &, const Product &);
        friend std::istream& operator >>(std::istream &, Product &);
        virtual std::string getName()const = 0;
        virtual std::string getFirm()const = 0;
        virtual std::string getCountry()const = 0;
        virtual int getPrice()const = 0;
        virtual int iAm()const = 0;
        virtual int volume()const = 0;
        virtual int cost(int)const = 0;
        virtual int reduceGood(int)const = 0;
        virtual Product& setName(std::string) = 0;
        virtual Product& setQuantity(int) = 0;
        virtual Product& setFirm(std::string) = 0;
        virtual Product& setCountry(std::string) = 0;
        virtual Product& setPrice(int) = 0;
        virtual Product& sett(int) = 0; // меняет размер опта или торговую надбавку
        virtual Product& supplementProducts(int) = 0;
        virtual Product& reduceProducts(int) = 0;
        virtual ~Product(){}
    };

    class Wholesale :public Product {
    protected:
        std::string name;
        int quantity;
        std::string firm;
        std::string country;
        int price;
        int wSize;
        virtual std::ostream& show(std::ostream&)const;
        virtual std::istream& get(std::istream&);
    public:
        Wholesale() = default;
        virtual std::string getName() const{ return name; }
        virtual std::string getFirm() const{ return firm; }
        virtual std::string getCountry() const{ return country; }
        virtual int getPrice() const{ return price; }
        virtual Wholesale* clone() const{ return new Wholesale(*this); }
        virtual int iAm()const { return 1; }
        virtual int volume() const;
        virtual int cost(int) const;
        virtual int reduceGood(int) const;
        virtual Wholesale& setName(std::string);
        virtual Wholesale& setQuantity(int);
        virtual Wholesale& setFirm(std::string);
        virtual Wholesale& setCountry(std::string);
        virtual Wholesale& setPrice(int);
        virtual Wholesale& sett(int);
        virtual Wholesale& supplementProducts(int);
        virtual Wholesale& reduceProducts(int);
    };

    class Retail :public Product {
    protected:
        std::string name;
        int quantity;
        std::string firm;
        std::string country;
        int price;
        int markup;
        virtual std::ostream& show(std::ostream&)const;
        virtual std::istream& get(std::istream&);
    public:
        Retail() = default;
        virtual std::string getName() const{ return name; }
        virtual std::string getFirm() const{ return firm; }
        virtual std::string getCountry() const{ return country; }
        virtual int getPrice() const{ return price; }
        virtual Retail* clone() const{ return new Retail(*this); }
        virtual int iAm()const { return 2; }
        virtual int volume() const;
        virtual int cost(int) const;
        virtual int reduceGood(int) const;
        virtual Retail& setName(std::string);
        virtual Retail& setQuantity(int);
        virtual Retail& setFirm(std::string);
        virtual Retail& setCountry(std::string);
        virtual Retail& setPrice(int);
        virtual Retail& sett(int);
        virtual Retail& supplementProducts(int);
        virtual Retail& reduceProducts(int);
    };
}

#endif