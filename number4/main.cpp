#include <string>
#include <iostream>
#include "table.h"
#include "product.h"

using namespace Products;
using namespace Prog4;

const std::string Names[] = { "Unknown", "Wholesale", "Retail" };
const std::string Menu[] = { "1. Add a product", "2. Find a product",
                             "3. Delete a product","4. Show all",
                             "5. Change a price", "6. Change a size of the wholesale or a trade markup",
                             "7. Supplement a product","8. Sell products",
                             "9. Transfer products","0. Quit" },
                Choice = "Make your choice",
                Msg = "You are wrong; repeat please";

int Answer(const std::string alt[], int n);
int Add(Table &), Find(Table &), Remove(Table &), ShowAll(Table &), ChWSorMar(Table &), ChPrise(Table &), Supplement(Table &), Sell(Table &), Transfer(Table &);
int(*Funcs[])(Table &) = { nullptr, Add, Find, Remove, ShowAll, ChPrise, ChWSorMar, Supplement, Sell, Transfer };

const int Num = sizeof(Menu)/sizeof(Menu[0]);

int main() {
    Table ar;
    int ind;
    try {
        while (ind = Answer(Menu, Num))
            Funcs[ind](ar);
        std::cout << "That's all. Buy!" << std::endl;
    }
    catch (const std::exception &er) {
        std::cout << er.what() << std::endl;
    }
    return 0;
}

int Answer(const std::string alt[], int n) {
    int answer;
    std::string prompt = Choice;
    std::cout << "\nWhat do you want to do:" << std::endl;
    for(int i = 0; i < n; i++)
        std::cout << alt[i].c_str() << std::endl;
    do{
        std::cout << prompt.c_str() << ": -> ";
        prompt = Msg;
        std::cin >> answer;
        if (!std::cin.good()){
            std::cout << "Error when number of choice was entered; \nRepeat, please." << std::endl;
            std::cin.ignore(80, '\n');
            std::cin.clear();
        }
    } while (answer < 0 || answer >= n);
    std::cin.ignore(80, '\n');
    return answer;
}

const std::string Sh[] = {"1. Wholesale", "2. Retail", "0. Quit"};
const int NumSh = sizeof(Sh) / sizeof(Sh[0]);

int Add(Table &a) {
    Product *ptr = nullptr;
    Wholesale w;
    Retail r;
    int code;
    int ans;
    while (ans = Answer(Sh, NumSh)) {
        std::cout << "Enter a product code: --> ";
        std::cin >> code;
        if (!std::cin.good())
            throw std::runtime_error("Error when a product code was entered");
        switch (ans){
            case 1:
                std::cout << "Enter values:\nstring(name) int(quantity) string(firm) string (country) int(price) int(wSize)\n--> ";
                ptr = &w;
                break;
            case 2:
                std::cout << "Enter values:\nstring(name) int(quantity) string(firm) string (country) int(price) int(markup)\n--> ";
                ptr = &r;
                break;
        }
        std::cin >> (*ptr);
        if (!std::cin.good())
            throw std::runtime_error("Error when shape values were entered");
        std::cin.ignore(80, '\n');
        if (a.insert(code, ptr))
            std::cout << "Ok" << std::endl;
        else
            std::cout << "Duplicate code" << std::endl;
    }
    return 0;
}

int Find(Table &a) {
    int code;
    Table::Const_Iterator it;
    const Product *ptr = nullptr;
    std::cout << "Enter a product code: --> ";
    std::cin >> code;
    if (!std::cin.good())
        throw std::runtime_error("Error when a product code was entered");
    it = a.find(code);
    if(it == a.end()){
        std::cout << "The Product with code \"" << code << "\" is absent in container"
                  << std::endl;
        return -1;
    }
    ptr = (*it).second;
    std::cout << "The Product with code \"" << (*it).first
              << "\" is a " << Names[ptr->iAm()]
              << std::endl;
    std::cout << (*ptr) << std::endl;
    std::cout << "It's volume is equal to " << ptr->volume() << std::endl;
    return 0;
}

int Remove(Table &a) {
    int code;
    std::cout << "Enter a product code: --> ";
    std::cin >> code;
    if (!std::cin.good())
        throw std::runtime_error("Error when a product code was entered");
    if (a.remove(code))
        std::cout << "Ok" << std::endl;
    else
        std::cout << "The Product with code \"" << code << "\" is absent in container"
                  << std::endl;
    return 0;
}

int ShowAll(Table &a) {
    Table::Const_Iterator it;
    for(it = a.begin(); it != a.end(); ++it)
        std::cout << (*it) << std::endl;
    return 0;
}

int ChPrise(Table &a) {
    int code;
    Table::Const_Iterator it;
    Product *ptr = nullptr;
    std::cout << "Enter a product code: --> ";
    std::cin >> code;
    if (!std::cin.good())
        throw std::runtime_error("Error when a product code was entered");
    it = a.find(code);
    if(it == a.end()){
        std::cout << "The Product with code \"" << code << "\" is absent in container"
                  << std::endl;
        return -1;
    }
    ptr = (*it).second;
    std::cout << "The Product with code \"" << (*it).first
              << "\" is a " << Names[ptr->iAm()]
              << std::endl;
    int newPrice;
    std::cout << "Enter a new price: --> ";
    std::cin >> newPrice;
    if (!std::cin.good())
        throw std::runtime_error("Error when a new price was entered");
    ptr->setPrice(newPrice);
    return 0;
}

int ChWSorMar(Table &a) {
    int code;
    Table::Const_Iterator it;
    Product *ptr = nullptr;
    std::cout << "Enter a product code: --> ";
    std::cin >> code;
    if (!std::cin.good())
        throw std::runtime_error("Error when a product code was entered");
    it = a.find(code);
    if(it == a.end()){
        std::cout << "The Product with code \"" << code << "\" is absent in container"
                  << std::endl;
        return -1;
    }
    ptr = (*it).second;
    std::cout << "The Product with code \"" << (*it).first
              << "\" is a " << Names[ptr->iAm()]
              << std::endl;
    int x;
    if (ptr->iAm() == 1) {
        std::cout << "Enter a new wholesale size: --> ";
        std::cin >> x;
        if (!std::cin.good())
            throw std::runtime_error("Error when a new wholesale size was entered");
    } else {
        std::cout << "Enter a new markup: --> ";
        std::cin >> x;
        if (!std::cin.good())
            throw std::runtime_error("Error when a new markup was entered");
    }
    ptr->sett(x);
    return 0;
}

int Supplement(Table &a) {
    int code;
    Table::Const_Iterator it;
    Product *ptr = nullptr;
    std::cout << "Enter a product code: --> ";
    std::cin >> code;
    if (!std::cin.good())
        throw std::runtime_error("Error when a product code was entered");
    it = a.find(code);
    if(it == a.end()){
        std::cout << "The Product with code \"" << code << "\" is absent in container"
                  << std::endl;
        return -1;
    }
    ptr = (*it).second;
    std::cout << "The Product with code \"" << (*it).first
              << "\" is a " << Names[ptr->iAm()]
              << std::endl;
    int x;
    if (ptr->iAm() == 1) {
        std::cout << "Enter a quantity of wholesale lots: --> ";
        std::cin >> x;
        if (!std::cin.good())
            throw std::runtime_error("Error when a quantity of wholesale lots was entered");
    } else {
        std::cout << "Enter a quantity of products: --> ";
        std::cin >> x;
        if (!std::cin.good())
            throw std::runtime_error("Error when a quantity of products was entered");
    }
    ptr->supplementProducts(x);
    return 0;
}

int Sell(Table &a) {
    int code;
    Table::Const_Iterator it;
    Product *ptr = nullptr;
    std::cout << "Enter a product code: --> ";
    std::cin >> code;
    if (!std::cin.good())
        throw std::runtime_error("Error when a product code was entered");
    it = a.find(code);
    if(it == a.end()){
        std::cout << "The Product with code \"" << code << "\" is absent in container"
                  << std::endl;
        return -1;
    }
    ptr = (*it).second;
    std::cout << "The Product with code \"" << (*it).first
              << "\" is a " << Names[ptr->iAm()]
              << std::endl;
    int x;
    if (ptr->iAm() == 1) {
        std::cout << "Enter a quantity of wholesale lots: --> ";
        std::cin >> x;
        if (!std::cin.good())
            throw std::runtime_error("Error when a quantity of wholesale lots was entered");
    } else {
        std::cout << "Enter a quantity of products: --> ";
        std::cin >> x;
        if (!std::cin.good())
            throw std::runtime_error("Error when a quantity of products was entered");
    }
    if (ptr->reduceGood(x) == 1) {
        ptr->reduceProducts(x);
        std::cout << "Profit received: " << ptr->cost(x) << std::endl;
    } else {
        std::cout << "The products are not sold. There are not enough products" << std::endl;
    }
    return 0;
}

int Transfer(Table &a) {
    int code;
    Table::Const_Iterator it;
    Product *ptr = nullptr;
    std::cout << "Enter a product code: --> ";
    std::cin >> code;
    if (!std::cin.good())
        throw std::runtime_error("Error when a product code was entered");
    it = a.find(code);
    if(it == a.end()){
        std::cout << "The Product with code \"" << code << "\" is absent in container"
                  << std::endl;
        return -1;
    }
    ptr = (*it).second;
    std::cout << "The Product with code \"" << (*it).first
              << "\" is a " << Names[ptr->iAm()]
              << std::endl;
    int x;
    std::cout << "Enter a quantity of products: --> ";
    std::cin >> x;
    if (!std::cin.good())
        throw std::runtime_error("Error when a quantity of products was entered");
    if (ptr->volume() >= x) {
        int newCode;
        Wholesale w;
        Retail r;
        Product *newPtr = nullptr;
        std::cout << "Enter a new product code: --> ";
        std::cin >> newCode;
        if (!std::cin.good())
            throw std::runtime_error("Error when a new product code was entered");
        int z;
        if (ptr->iAm() == 1) {
            newPtr = &r;
            std::cout << "Enter a markup: --> ";
            std::cin >> z;
            if (!std::cin.good())
                throw std::runtime_error("Error when a markup was entered");
        } else {
            newPtr = &w;
            std::cout << "Enter a wholesale size: --> ";
            std::cin >> z;
            if (!std::cin.good())
                throw std::runtime_error("Error when a wholesale size was entered");
        }
        newPtr->setName(ptr->getName());
        newPtr->setQuantity(x);
        newPtr->setFirm(ptr->getFirm());
        newPtr->setCountry(ptr->getCountry());
        newPtr->setPrice(ptr->getPrice());
        newPtr->sett(z);
        if (a.insert(newCode, newPtr))
            std::cout << "Ok" << std::endl;
        else {
            std::cout << "Duplicate code" << std::endl;
            return -1;
        }
        ptr->setQuantity(ptr->volume()-x);
    } else {
        std::cout << "The products are not transfer. There are not enough products" << std::endl;
    }
    return 0;
}