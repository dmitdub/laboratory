#include <string>
#include <iostream>
#include "table.h"
#include "product.h"

using namespace Products;
using namespace Prog4;

const std::string Names[] = { "Unknown", "Wholesale", "Retail" };
const std::string Menu[] = { "1. Add a product", "2. Find a product",
                             "3. Delete a product","4. Show all",
                             "0. Quit" },
        Choice = "Make your choice",
        Msg = "You are wrong; repeat please";

int Answer(const std::string alt[], int n);
int Add(Table &), Find(Table &), Remove(Table &), ShowAll(Table &);
int(*Funcs[])(Table &) = { nullptr, Add, Find, Remove, ShowAll };

const int Num = sizeof(Menu)/sizeof(Menu[0]);

int main() {
    Table ar;
    int ind;
    try {
        while (ind = Answer(Menu, Num))
            Funcs[ind](ar);
        std::cout << "That's all. Buy!" << std::endl;
    }
    catch (const std::exception &er) {
        std::cout << er.what() << std::endl;
    }
    return 0;
}

int Answer(const std::string alt[], int n) {
    int answer;
    std::string prompt = Choice;
    std::cout << "\nWhat do you want to do:" << std::endl;
    for(int i = 0; i < n; i++)
        std::cout << alt[i].c_str() << std::endl;
    do{
        std::cout << prompt.c_str() << ": -> ";
        prompt = Msg;
        std::cin >> answer;
        if (!std::cin.good()){
            std::cout << "Error when number of choice was entered; \nRepeat, please." << std::endl;
            std::cin.ignore(80, '\n');
            std::cin.clear();
        }
    } while (answer < 0 || answer >= n);
    std::cin.ignore(80, '\n');
    return answer;
}

const std::string Sh[] = {"1. Wholesale", "2. Retail", "0. Quit"};
const int NumSh = sizeof(Sh) / sizeof(Sh[0]);

int Add(Table &a) {
    Product *ptr = nullptr;
    Wholesale w;
    Retail r;
    int code;
    int ans;
    while (ans = Answer(Sh, NumSh)) {
        std::cout << "Enter a product code: --> ";
        std::cin >> code;
        if (!std::cin.good())
            throw std::runtime_error("Error when a product code was entered");
        switch (ans){
            case 1:
                std::cout << "Enter values:\nstring(name) int(quantity) string(firm) string (country) int(price) int(wSize)\n--> ";
                ptr = &w;
                break;
            case 2:
                std::cout << "Enter values:\nstring(name) int(quantity) string(firm) string (country) int(price) int(markup)\n--> ";
                ptr = &r;
                break;
        }
        std::cin >> (*ptr);
        if (!std::cin.good())
            throw std::runtime_error("Error when shape values were entered");
        std::cin.ignore(80, '\n');
        if (a.insert(code, ptr))
            std::cout << "Ok" << std::endl;
        else
            std::cout << "Duplicate code" << std::endl;
    }
    return 0;
}

int Find(Table &a) {
    int code;
    Table::Const_Iterator it;
    const Product *ptr = nullptr;
    std::cout << "Enter a product code: --> ";
    std::cin >> code;
    if (!std::cin.good())
        throw std::runtime_error("Error when a product code was entered");
    it = a.find(code);
    if(it == a.end()){
        std::cout << "The Product with code \"" << code << "\" is absent in container"
                  << std::endl;
        return -1;
    }
    ptr = (*it).second;
    std::cout << "The Product with code \"" << (*it).first
              << "\" is a " << Names[ptr->iAm()]
              << std::endl;
    std::cout << (*ptr) << std::endl;
    std::cout << "It's volume is equal to " << ptr->volume() << std::endl;
    return 0;
}

int Remove(Table &a) {
    int code;
    const Product *ptr = nullptr;
    std::cout << "Enter a product code: --> ";
    std::cin >> code;
    if (!std::cin.good())
        throw std::runtime_error("Error when a product code was entered");
    if (a.remove(code))
        std::cout << "Ok" << std::endl;
    else
        std::cout << "The Product with code \"" << code << "\" is absent in container"
                  << std::endl;
    return 0;
}

int ShowAll(Table &a) {
    Table::Const_Iterator it;
    for(it = a.begin(); it != a.end(); ++it)
        std::cout << (*it) << std::endl;
    return 0;
}