#include <iostream>
#include "product.h"

namespace Products {
    std::ostream& Wholesale::show(std::ostream& os)const {
        return os << "Name: " << name
            << " ; Quantity: " << quantity
            << " ; Firm: " << firm
            << " ; Country: " << country
            << " ; Price: " << price
            << " ; wSize: " << wSize;
    }

    std::ostream& Retail::show(std::ostream& os)const {
        return os << "Name: " << name
                  << "; Quantity: " << quantity
                  << "; Firm: " << firm
                  << "; Country: " << country
                  << "; Price: " << price
                  << "; markup: " << markup;
    }

    std::ostream& operator <<(std::ostream &os, const Product &p) {
        return p.show(os);
    }

    std::istream& Wholesale::get(std::istream& is) {
        return is >> name >> quantity >> firm >> country >> price >> wSize;
    }

    std::istream& Retail::get(std::istream& is) {
        return is >> name >> quantity >> firm >> country >> price >> markup;
    }

    std::istream& operator >>(std::istream &is, Product &p) {
        return p.get(is);
    }

    int Wholesale::volume() const {
        return quantity;
    }

    int Retail::volume() const {
        return quantity;
    }

    int Wholesale::cost(int x) const {
        return x*wSize*price;
    }

    int Retail::cost(int x) const {
        return x*quantity*(1+markup/100);
    }

    int Wholesale::reduceGood(int x) const {
        if (quantity >= x*wSize)
            return 1;
        else return -1;
    }

    int Retail::reduceGood(int x) const {
        if (quantity >= x)
            return 1;
        else return -1;
    }

    Wholesale& Wholesale::setName(std::string n0) {
        name = n0;
        return *this;
    }

    Retail& Retail::setName(std::string n0) {
        name = n0;
        return *this;
    }

    Wholesale& Wholesale::setQuantity(int q0) {
        quantity = q0;
        return *this;
    }

    Retail& Retail::setQuantity(int q0) {
        quantity = q0;
        return *this;
    }

    Wholesale& Wholesale::setFirm(std::string f0) {
        firm = f0;
        return *this;
    }

    Retail& Retail::setFirm(std::string f0) {
        firm = f0;
        return *this;
    }

    Wholesale& Wholesale::setCountry(std::string c0) {
        country = c0;
        return *this;
    }

    Retail& Retail::setCountry(std::string c0) {
        country = c0;
        return *this;
    }

    Wholesale& Wholesale::setPrice(int p0) {
        price = p0;
        return *this;
    }

    Retail& Retail::setPrice(int p0) {
        price = p0;
        return *this;
    }

    Wholesale& Wholesale::sett(int wS0) {
        wSize = wS0;
        return *this;
    }

    Retail& Retail::sett(int m0) {
        markup = m0;
        return *this;
    }

    Wholesale& Wholesale::supplementProducts(int q0) {
        quantity += q0*wSize;
        return *this;
    }

    Retail& Retail::supplementProducts(int q0) {
        quantity += q0;
        return *this;
    }

    Wholesale& Wholesale::reduceProducts(int q0) {
        quantity -= q0*wSize;
        return *this;
    }

    Retail& Retail::reduceProducts(int q0) {
        quantity -= q0;
        return *this;
    }
}