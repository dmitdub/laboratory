#include "cardioid.h"
#include <string.h>
#include <stdexcept>

namespace Prog {
    double Cardioid::distance(double theta) const {
        if (theta < 0 or theta > 360)
            throw std::out_of_range("invalid theta");
        return std::abs(2*parameter*(1-cos(theta*M_PI/180)));
    }

    double Cardioid::curvature(double theta) const {
        if (theta < 0 or theta > 360)
            throw std::out_of_range("invalid theta");
        return std::abs((8.0/3)*parameter*sin(theta*M_PI/360));
    }

    double Cardioid::length(double theta) const {
        if (theta < 0 or theta > 360)
            throw std::out_of_range("invalid theta");
        return std::abs(8*parameter*(1-cos(theta*M_PI/360)));
    }

    char *Cardioid::frm() const // (x^2 + y^2 + |2a|x)^2 - |4a^2| * (x^2 + y^2) = 0
    {
        int l = 13 + 7 + 18 + 1;
        char num[20];
        sprintf_s(num, 20, "%.2f", 2*parameter);
        l += strlen(num);
        sprintf_s(num, 20, "%.2f", 4*parameter*parameter);
        l += strlen(num);
        char *s = new char[l];
        if (parameter == 0)
            sprintf_s(s, l, "x^2 + y^2 = 0");
        else
            sprintf_s(s, l, "(x^2 + y^2 + %.2fx)^2 - %.2f * (x^2 + y^2) = 0", 2*parameter, 4*parameter*parameter);
        return s;
    }
}
