#ifndef _CARDIOID_H_
#define _CARDIOID_H_
#include "../Google_tests/lib/googletest/include/gtest/gtest.h"
#include <cmath>

namespace Prog {
    class Cardioid { // (x^{2}+y^{2}+2ax)^{2}-4a^{2}*(x^{2}+y^{2})=0   or   r=2a*(1-cos(theta))
    private:
        double parameter;
    public:
        explicit Cardioid(double prmtr = 1) { parameter = prmtr; }
        Cardioid& setParameter(double parameter0){ parameter = parameter0; return *this; }
        [[nodiscard]] double getParameter() const{ return parameter; }
        [[nodiscard]] double distance(double theta) const;
        [[nodiscard]] double maxY() const{ return std::abs(2*parameter*sin(2*M_PI/3)-parameter*sin(4*M_PI/3)); }
        [[nodiscard]] double curvature(double theta) const;
        [[nodiscard]] double area() const{ return 6*M_PI*parameter*parameter; }
        [[nodiscard]] double length(double theta) const;
        [[nodiscard]] char *frm() const;
    };
}

#endif
