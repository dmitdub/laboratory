#include "cardioid.h"
#include <iostream>

int main() {
    Prog::Cardioid c(1);
    int fl1 = 1;
    double prmtr;
    char *s = NULL;
    while (fl1) {
        std::cout << "Your cardioid is:" << std::endl;
        s = c.frm();
        std::cout << s << std::endl;
        delete [] s;
        std::cout << "Parameter: " << c.getParameter() << std::endl;
        std::cout << "area: " << c.area() << std::endl;
        std::cout << "maxY: " << c.maxY() << std::endl;
        int fl2 = 1;
        while (fl2) {
            int tht;
            std::cout << "Enter theta (degrees) for calculate value distance(theta), curvature(theta) and length(theta):" << std::endl;
            std::cin >> tht;
            fl2 = std::cin.good();
            if(!fl2)
                continue;
            try {
                double distance = c.distance(tht);
                double curvature = c.curvature(tht);
                double length = c.length(tht);
                std::cout << "distance = " << distance << std::endl;
                std::cout << "curvature = " << curvature << std::endl;
                std::cout << "length = " << length << std::endl;
            }
            catch (std::exception &ex) {
                std::cout << ex.what() << std::endl;
            }
        }
        std::cin.clear();
        std::cin.sync();
        std::cout << "Enter new parameter to continue:" << std::endl;
        std::cin >> prmtr;
        if (std::cin.good()) {
            c.setParameter(prmtr);
        }
        else
            fl1 = 0;
    }
    return 0;
}
