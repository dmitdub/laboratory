#include "gtest/gtest.h"
#include "../src/cardioid.h"

TEST(CardioidConstructor, DefaultConstructor)
{
    Prog::Cardioid a1;
    ASSERT_EQ(1, a1.getParameter());
}

TEST(CardioidConstructor, InitConstructors)
{
    Prog::Cardioid a2(3);
    ASSERT_EQ(3, a2.getParameter());
}

TEST(CardioidMethods, Setters)
{
    Prog::Cardioid a;
    a.setParameter(2);
    ASSERT_EQ(2, a.getParameter());
}

TEST(CardioidMethods, Parameters)
{
    Prog::Cardioid a1;
    const double err = 0.00001;
    ASSERT_NEAR(6*M_PI, a1.area(), err);
    ASSERT_NEAR(3*sqrt(3)/2, a1.maxY(), err);
    ASSERT_EQ(0, a1.distance(360));
    ASSERT_NEAR(0, a1.curvature(360), err);
    ASSERT_EQ(16, a1.length(360));
    ASSERT_STREQ("(x^2 + y^2 + 2.00x)^2 - 4.00 * (x^2 + y^2) = 0", a1.frm());
    a1.setParameter(0);
    ASSERT_STREQ("x^2 + y^2 = 0", a1.frm());
    a1.setParameter(-3);
    ASSERT_NEAR(54*M_PI, a1.area(), err);
    ASSERT_NEAR(9*sqrt(3)/2, a1.maxY(), err);
    ASSERT_NEAR(3, a1.distance(60), err);
    ASSERT_NEAR(4, a1.curvature(60), err);
    ASSERT_NEAR(24-12*sqrt(3), a1.length(60), err);
    ASSERT_STREQ("(x^2 + y^2 + -6.00x)^2 - 36.00 * (x^2 + y^2) = 0", a1.frm());
    ASSERT_ANY_THROW(a1.distance(540));
    ASSERT_ANY_THROW(a1.curvature(-180));
    ASSERT_ANY_THROW(a1.length(360.1));
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
